package abi.mehran.guessgame

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.*

class MainActivity : AppCompatActivity() {
    private var progressVal: Int = 50
    private var doRestart: Boolean = false
    private var number: Int = 0

    private lateinit var sliderTv: TextView

    private lateinit var display: LinearLayout
    private lateinit var disNum1: TextView
    private lateinit var disNum2: TextView

    private lateinit var status: TextView
    private lateinit var inputLayout: LinearLayout
    private lateinit var input: EditText
    private lateinit var check: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sliderTv = findViewById(R.id.slider_tv)

        val numberSlider: SeekBar = findViewById(R.id.number_slider)
        numberSlider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progressVal = progress * 10
                updateSliderTv()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        val startBtn: Button = findViewById(R.id.start_btn)
        startBtn.setOnClickListener { v ->
            if (doRestart) {
                numberSlider.isEnabled = true
                startBtn.setText(R.string.select_button)
                resetDisplayTVs()
                resetStatusTV()
                inputLayout.visibility = LinearLayout.VISIBLE
                display.visibility = LinearLayout.GONE
                doRestart = false
            } else {
                number = generateNumber()
                v.isEnabled = false
                numberSlider.isEnabled = false
                runNumDisplay()
            }
        }

        display = findViewById(R.id.display)
        disNum1 = findViewById(R.id.num1)
        disNum2 = findViewById(R.id.num2)

        status = findViewById(R.id.status_tv)
        inputLayout = findViewById(R.id.input_layout)
        input = findViewById(R.id.input)

        check = findViewById(R.id.check_btn)
        check.setOnClickListener {
            val guess: String = input.text.toString()
            if (guess.isEmpty()) {
                Toast.makeText(applicationContext, "Type your guess!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val guessNum: Int = guess.toInt()
            when {
                guessNum < number -> {
                    status.setText(R.string.higher_guess)
                    status.setTextColor(Color.RED)
                }
                guessNum > number -> {
                    status.setText(R.string.lower_guess)
                    status.setTextColor(Color.RED)
                }
                else -> {
                    status.setText(R.string.correct_guess)
                    status.setTextColor(Color.GREEN)

                    disNum1.text = number.toString()
                    disNum1.setTextColor(Color.GREEN)
                    disNum2.visibility = TextView.GONE

                    inputLayout.visibility = LinearLayout.GONE

                    startBtn.isEnabled = true
                    startBtn.setText(R.string.restart_button)
                    doRestart = true
                }
            }
        }
    }

    private fun resetStatusTV() {
        status.setTextColor(R.color.colorPrimaryDark)
        status.setText(R.string.put_guess)
    }

    private fun resetDisplayTVs() {
        disNum1.text = ""
        disNum2.text = ""
        disNum1.setTextColor(Color.BLACK)
        disNum2.setTextColor(Color.BLACK)
        disNum1.visibility = TextView.VISIBLE
        disNum2.visibility = TextView.VISIBLE
    }

    private fun updateSliderTv() {
        sliderTv.text = progressVal.toString()
    }

    private fun runNumDisplay() {
        display.visibility = LinearLayout.VISIBLE

        val timer: CountDownTimer = object : CountDownTimer(1000, 20) {
            override fun onFinish() {
                disNum1.text = "?"
                disNum2.text = "?"
            }

            override fun onTick(millisUntilFinished: Long) {
                disNum1.text = generateRandom()
                disNum2.text = generateRandom()
            }
        }

        timer.start()
    }

    private fun generateRandom(): String {
        return (0..9).random().toString()
    }

    private fun generateNumber(): Int {
        return (0..progressVal).random()
    }
}